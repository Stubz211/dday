import os
import requests


def parser(number=1):
    for i in range(number):
        url = 'https://source.unsplash.com/category/food'
        response = requests.get(url)
        img_index = 0
        while True:
            if os.path.exists('img_{}.jpg'.format(img_index)):
                img_index += 1
            elif os.path.exists('img_{}.jpg'.format(img_index)):
                continue
            else:
                break
        img = open('{}.jpg'.format('img_'+str(img_index)), 'wb')
        img.write(response.content)
        img.close()

parser(10)