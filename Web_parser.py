import urllib.request
from bs4 import BeautifulSoup

base_url = 'http://www.sql-ex.ru/help/select0.php?Lang=0'


def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()


def parse(html):
    soup = BeautifulSoup(html, 'lxml')
    table = soup.find_all('a', class_='let')
    for row in table:
        x = str(row).find('>')
        print('http://'+str(row)[21:x], row.text)


def main():
    parse(get_html(base_url))


if __name__ == '__main__':
    main()
