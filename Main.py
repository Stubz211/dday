# -*- coding: UTF-8 -*-
from Tkinter import *
import tkMessageBox
import Test


# define support programs
def get_help():
    tkMessageBox.showinfo('Help', """
    Вас приветсвует Newpaper Helper Принцип работы: нужно ввести необходимые данные в строчку (разделяя пробелом) и\
    выбрать соотвествующую кнопку
    Пример:1. нанять сотрудника в строку написать - Имя, фамилию, id района, (тип доходности) нажать add employee
    2. Уволить ввести  - id работника нажать fire
    3. Нанять, вести - id бывшего работника нажать hire
    4. изменить тип доходности - id работника и указать тип доходности (1 - на каждую тысячу, 2 - стабильный, 3 - \
    процентный) нажать benefit
    5. Добавить район, ввести - Название, Количество населения нажать add district
    6. Изменить район, ввести - id района, Название, Кол-во населения нажать change district
    7. добавить рабочих дней, ввести - id работника, день, год, месяц, часы начала работы, часы конца работы, id района\
    нажать add work days
    8.Изменить рабочяие дни, ввести - id работника, день, год, месяц, часы начала работы, часы конца работы, id района\
    нажать chenge work days
    9. удалить рабочий день, ввести - id работника
    """)


def error():
    tkMessageBox.showerror('Oops', 'something wrong... press "HELP" !')


def add_employee():
    try:
        arg = str(entry.get()).split(' ')
        Test.create_employee(*arg)
    except:
        error()


def fire_employee():
    try:
        arg = str(entry.get()).split(' ')
        Test.fire_employee(*arg)
    except:
        error()


def hire_employee():
    try:
        arg = str(entry.get()).split(' ')
        Test.hire_employee(*arg)
    except:
        error()


def change_employee_benefit():
    try:
        arg = str(entry.get()).split(' ')
        Test.change_employee_benefit(*arg)
    except:
        error()


def add_district():
    try:
        arg = str(entry.get()).split(' ')
        Test.add_district(*arg)
    except:
        error()


def change_district():
    try:
        arg = str(entry.get()).split(' ')
        Test.change_district(*arg)
    except:
        error()


def add_work_days():
    try:
        arg = str(entry.get()).split(' ')
        Test.add_work_days(*arg)
    except:
        error()


def change_work_days():
    try:
        arg = str(entry.get()).split(' ')
        Test.change_work_days(*arg)
    except:
        error()


def del_work_days():
    try:
        arg = str(entry.get()).split(' ')
        Test.del_work_days(*arg)
    except:
        error()


# Tkinter module
root = Tk()
root.title('Newspaper Helper')
root.geometry('400x300')

# Entry
entry = Entry(root)
entry.focus_set()
entry.place(x=10, y=10, width=250, height=30)

# Buttons & Labels
Label(root, text='Действия с рабониками').place(x=15, y=50, width=150)
Button(root, text='Add employee', command=add_employee).place(x=10, y=70, width=120)
Button(root, text='fire', command=fire_employee).place(x=140, y=70, width=60)
Button(root, text='Hire', command=hire_employee).place(x=200, y=70, width=60)
Button(root, text='benefit', command=change_employee_benefit).place(x=260, y=70, width=50)

Label(root, text='Действия с районами').place(x=15, y=100, width=140)
Button(root, text='add district', command=add_district).place(x=10, y=120, width=120)
Button(root, text='change district', command=change_district).place(x=140, y=120, width=120)

Label(root, text='Действия с рабочими днями').place(x=15, y=150, width=180)
Button(root, text='add work days', command=add_work_days).place(x=10, y=170, width=120)
Button(root, text='change work days', command=change_work_days).place(x=130, y=170, width=120)
Button(root, text='delete work days', command=del_work_days).place(x=250, y=170, width=120)
# Help
Button(root, text='Help', command=get_help).place(x=10, y=250, width=100)
# looping frame
root.mainloop()
