# -*- coding: utf-8 -*-
import random
import sqlite3

con = sqlite3.connect('fast_news.db')
cur = con.cursor()

table_employee = """CREATE TABLE IF NOT EXISTS employee (
  id          INTEGER      NOT NULL PRIMARY KEY,
  first_name  VARCHAR(128) NOT NULL,
  last_name   VARCHAR(128) NOT NULL,
  is_working  INTEGER,
  district_id INTEGER DEFAULT -1,
  benefit_type INTEGER NOT NULL DEFAULT 1
  );
"""
table_district = """CREATE TABLE IF NOT EXISTS district (
  district_id         INTEGER NOT NULL PRIMARY KEY,
  name       VARCHAR(128),
  population INTEGER NOT NULL
);
"""
table_work_days = """CREATE TABLE IF NOT EXISTS work_days (
  employee_id INTEGER NOT NULL,
  day INTEGER NOT NULL,
  month INTEGER NOT NULL,
  year INTEGER NOT NULL,
  hour_start INTEGER NOT NULL,
  hour_end INTEGER NOT NULL,
  district_id INTEGER NOT NULL
);
"""
# Create 3 tables
def create_tables():
    for table_def in (table_employee,table_district,table_work_days):
        cur.execute(table_def)
        con.commit()

# =====================================
# function with employee
# =====================================
def create_employee(first_name, last_name, district_id=-1, benefit_type=1):
    cur.execute("""INSERT INTO employee (first_name, last_name, is_working, district_id, benefit_type)
    VALUES ('{0}', '{1}', '1', {2}, {3});""".format(first_name, last_name, district_id, benefit_type))
    con.commit()
# fire_employee
def fire_employee(employee_id):
    cur.execute("""UPDATE employee SET is_working = 0 WHERE id = {0};""".format(employee_id))

    con.commit()
# hire employee
def hire_employee(employee_id):
    cur.execute("""UPDATE employee SET is_working = 1 WHERE id = {0};""".format(employee_id))
    con.commit()

# change employee benefit_type (1 = per thousand, 2 = stable, 3 = percentage )
def change_employee_benefit(employee_id, benefit_type):
    cur.execute("""UPDATE employee SET benefit_type = {1} WHERE id = {0};""".format(employee_id,benefit_type))
    con.commit()

# =====================================
# function with work_day
# =====================================
def add_work_days(employee_id, day, year, month, hour_start, hour_end, district_id):
    cur.execute("""INSERT INTO work_days (employee_id, day, year, month, hour_start, hour_end, district_id)
    VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6});"""
    .format(employee_id, day, year, month, hour_start, hour_end, district_id))
    con.commit()

def change_work_days(employee_id,day, year, month, hour_start, hour_end, district_id):
    cur.execute("""UPDATE work_days SET day =  '{0}', year = '{1}', month = '{2}', hour_start ='{3}',
    hour_end ='{4}', district_id ='{5}' WHERE employee_id = {6} );"""
    .format(day, year, month, hour_start, hour_end, district_id), employee_id)
    con.commit()

def del_work_days(employee_id):
    cur.execute("""DELETE FROM work_days WHERE employee_id ={0};
    """.format(employee_id))
    con.commit()

# =====================================
# function with district
# =====================================
def add_district(name, population):
    cur.execute("""INSERT INTO district (name, population)
    VALUES ('{0}', '{1}');""".format(name,population))
    con.commit()

def change_district(district_id, name, population):
    cur.execute("""UPDATE district SET name='{0}', population='{1}'
    WHERE district_id= {2};"""
    .format(name, population, district_id))
    con.commit()

# defintion chance (0-100)
def chance():
    return int(random.random()*100)

# defintion percentage
def percentage():
    chances = chance()
    if chances <= 60:
        return 800
    elif 60 < chances <= 83:
        return 1000
    elif chances > 83:
        return 1200


if __name__ == '__main__':
    create_tables()









