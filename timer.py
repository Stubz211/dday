import time, sys
reps = 10000
replist = range(reps)

def timer(func, *args, **kwargs):
    start = time.time()
    for i in replist:
        ret = func(*args, **kwargs)
    elapsed = time.time() - start
    return (elapsed, ret)

def forLoop():
    res =[]
    for i in replist:
        res.append(abs(i))
    return res

def listComp():
    return [abs(x) for x in replist]

def mapCall():
    return list(map(abs, replist))

def genExpr():
    return list(abs(x) for x in replist)

def genFunc():
    def gen():
        for x in replist:
            yield abs(x)
    return list(gen())

print(sys.version)

for test in (forLoop, listComp, mapCall, genExpr, genFunc):
    elapsed, res = timer(test)
    print('-'*33)
    print('%-9s: %.5f => [%s...%s]' %(test.__name__, elapsed, res[0], res[-1]))